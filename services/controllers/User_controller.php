<?php

class User_controller extends \Fox\FoxServiceController {

    function __construct() {
        parent::__construct();
    }

    public function getUser() {
        if($_GET["id"]){
            \Fox\Core\Penelope::printJSON(Usuario::getById($_GET["id"])->toArray());
        }else{
            \Fox\Core\Penelope::printJSON(Usuario::getAll());
        }
        
    }
    
    public function postUser() {
        $data = $_POST;
        $u = Usuario::instanciate($data);
        $r = $u->create();
        if($r["error"] == "0"){
            \Fox\Core\Request::setHeader(201);
        }
        \Fox\Core\Penelope::printJSON($r);
    }
    
    public function putUser(){
        $data = $this->_PUT;
        
        if(!isset($data["id"])){ \Fox\Core\Request::setHeader(400); exit();}
        
        $id = $data["id"];
        unset($data["id"]);
        
        if(count($data)<1){ \Fox\Core\Request::setHeader(200); exit(); }
        
        $u = Usuario::getById($id); //TODO check if user exists
        foreach ($data as $key => $value) {
            $u->{"set".ucfirst($key)}($value);
        }
        
        $r = $u->update();
        \Fox\Core\Penelope::printJSON($r);
    }
    
    public function deleteUser(){
        $data = $this->_DELETE;
    }

}
